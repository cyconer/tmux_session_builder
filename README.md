# tmux session builder

Create a tmux session script interactively from predefined commands.

If you want to create a tmux session that always contains the same layout and
always has the same programs executed on startup, writing a script 
that does exactly that is sufficient. But sometimes, executing all commands
is not desired. This tmux session builder interactively asks you
what commands should initially be executed, and persists that script as a file
which can then be executed to start the desired session.

## Example
The following asks you which commands should be executed, and then
creates a shell script at `/tmp/tmux_session_script.sh`. Executing
this script creates a tmux session with the name **example**. If all answers
are answered with yes, this script would then create two windows. In the first
window, the command `ls -a` is executed. In the second
window, two split windows exist, where the commands `firefox && exit` and
`thunderbird && exit` are executed, respectively.

```bash
#!/bin/bash
TMUXSCRIPT='/tmp/tmux_session_script.sh'
NAME='example'
JSON='{
    "abc": [
        ["ls", "ls -a"]
    ],
    "def": [
        ["firefox", "firefox && exit"],
        ["thunderbird", "thunderbird && exit"]
    ]
}'

tmux_session_builder.py "$TMUXSCRIPT" "$NAME" "$JSON"
```

**abc** and **def** will be the name of the windows. **ls**, **firefox** and
**thunderbird** are the displayed representations of the commands `ls -a`, `firefox && exit` and `thunderbird && exit` that would
be executed:
```bash
$ projects/tmux_session_builder/example_script.sh                           
ls [Y/n]: y
firefox [Y/n]: y
thunderbird [Y/n]: 
```


If you want to create and attach to this session immediately, you can use
```bash
tmux_session_builder.py "$TMUXSCRIPT" "$NAME" "$JSON" && chmod u+x "$TMUXSCRIPT" && "$TMUXSCRIPT"
```