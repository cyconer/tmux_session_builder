#!/bin/bash
TMUXSCRIPT='/tmp/tmux_session_script.sh'
NAME='example'
JSON='{
    "abc": [
        ["ls", "ls -a"]
    ],
    "def": [
        ["firefox", "firefox && exit"],
        ["thunderbird", "thunderbird && exit"]
    ]
}'

tmux_session_builder.py "$TMUXSCRIPT" "$NAME" "$JSON" && chmod u+x "$TMUXSCRIPT" && "$TMUXSCRIPT" && rm "$TMUXSCRIPT"
